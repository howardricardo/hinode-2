(function(){
	
	//ADDEVENTLISTNER SCROLL	
	$(window).scroll(function(){
		if($(window).scrollTop() > 0){

			//QUERYSELECTIOR ALL / FOREATCH  OU FOR
			$(".produtos li").each(function(){
    
    			//ELEMENT  OU ARRAYLIS[I]
			   let li = $(this)


			    if(visivel(li)){

			    	//ELEMENTO TEM ESSA CLASS
			    	if(li.hasClass('bloco')){
						li.removeClass('bloco')
						li.addClass('show')
					}
				} else {
					li.removeClass('show')
					li.addClass('bloco')
				}
			})
		}
	})

	$('#sanduiche').click(function() {
		if (!$('#options').hasClass('options-open')) {
			$('#options').slideDown()
			$('#options').addClass('options-open')
			$('#list').removeClass('list')
			$('#list').addClass('listVisivel')
			$(this).removeClass('sanduiche')
			$(this).addClass('sanduiche-open')
		} else {
			$('#options').removeClass('options-open')
			$('#list').removeClass('listVisivel')
			$('#list').addClass('list')
			$(this).removeClass('sanduiche-open')
			$(this).addClass('sanduiche')
		}
	})

	function visivel(elemento){

		let  tela = $(window);
		let  tela_atual = {
			top  : tela.scrollTop(),
			left : tela.scrollLeft()
		};
		
		tela_atual.right  = tela_atual.left + tela.width()
		tela_atual.bottom = tela_atual.top + tela.height()
		
		let limites = elemento.offset()
	
	    limites.right  = limites.left + elemento.outerWidth()
	    limites.bottom = limites.top  + elemento.outerHeight()
		
	    return (!(tela_atual.right < limites.left || tela_atual.left > limites.right || tela_atual.bottom < limites.top || tela_atual.top > limites.bottom))
	}
}())